<!--
The script at the path `docs.sh` in the project's directory generates this
document from certain comment lines in files at the paths

    main.yml
    roles/alpine_linux/tasks/main.yml

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    #### DOC BLOCK BEGIN

and ends with the exact line

    #### DOC BLOCK END

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one `#` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one `#` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document.
-->

Overview
--------

This Ansible playbook tries to ensure that a machine is a KVM/QEMU host.  It
does so by calling an Ansible role appropriate for the machine's operating
system.

The playbook assumes, but does not require, that the playbook `base_machine`
(https://levigo.de/stash/projects/LC/repos/ansible.playbooks.base_machine)
has already ensured that the machine is fully provisioned.  In fact, the two
playbooks are independent, and can be run or re-run in any order.  However,
for the closest approach to immutability, run `base_machine` once only then
`kvm_host` once only on each freshly provisioned machine.

The Alpine Linux role
---------------------

### Alpine Linux commands

Try to ensure that the machine is equipped with the current versions of the
helper commands, written specifically for Alpine Linux, that help ensure the
state of the machine.

### User and group for KVM/QEMU

In order to run KVM/QEMU guests as a user other than `root`, try to ensure
that the user and group `kvm` exist.

### File system for KVM/QEMU and libvirt

In order to provide a dedicated home for KVM/QEMU and libvirt, try to ensure
that an `ext4` file system on an active LVM2 logical volume comprising 16GiB
of the LVM2 volume group `levigo_fast` (if it exists) or `levigo` (otherwise)
is persistently mounted at the path `/kvm` with the permissions `0755` and
the owner and group `root`.

### Directory for KVM/QEMU backing volumes

In order to provide a local repository of KVM/QEMU backing volumes, try to
ensure that an immutable copy of each `qcow2` disk-image file in the Git
repository (https://levigo.de/stash/projects/LC/repos/images.virtual) of
virtual-machine images exists with the permissions `0440` and the owner and
group `kvm` in a directory at the path `/kvm/backing_volumes` with the
permissions `0755` and the owner and group `root`.

### File system for KVM/QEMU running volumes

In order to provide a dedicated storage area for KVM/QEMU running volumes,
try to ensure that an `ext4` file system on an active LVM2 logical volume
comprising the rest of the LVM2 volume group `levigo` is persistently mounted
at the path `/kvm/running-volumes` with the permissions `0755` and the owner
and group `kvm`.

### Directory for libvirt

In order to locate the dynamic components of libvirt outside of the root file
system, try to ensure that the paths `/var/cache/libvirt`, `/etc/libvirt` and
`/var/lib/libvirt` are symbolic links to the paths `/kvm/libvirt/cache`,
`/kvm/libvirt/etc` and `/kvm/libvirt/lib` respectively, each with the
permissions `0755` and the owner and group `root`.

### KVM/QEMU and libvirt

Try to ensure that the packages, modules and services necessary for KVM/QEMU
and libvirt are installed, loaded, configured and running.

### `kvm_tool`

Try to ensure that `kvm_tool`
(https://levigo.de/stash/projects/LC/repos/kvm.kvm_tool) is installed.

docs.sh
-------

The script at the path `docs.sh` in the project's directory generates this
document from certain comment lines in files at the paths

    main.yml
    roles/alpine_linux/tasks/main.yml

in the project's directory.  Edit the files and execute the script in order to
change this document.

Only comment lines that occur within a block that starts with the exact line

    #### DOC BLOCK BEGIN

and ends with the exact line

    #### DOC BLOCK END

contribute to this document.  A line in such a block comprising any number of
tab or space characters followed by one `#` character followed by one tab or
space character followed by a sequence of characters contributes the sequence
of characters.  A line in such a block comprising any number of tab or space
characters followed by one `#` character followed by a newline or a character
other than a tab or space character contributes an empty line.  No other line
contributes to this document.
